import org.json.JSONException;
import org.json.JSONObject;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		JSONObject json = new JSONObject();
		try {
			json.put("contestId", "121");
			json.put("startWord", "dog");
			json.put("endWord", "put");
			
			// JSON to Input
			Input input = new Input(json);
			Dictionary dict = new Dictionary();
			// Calculate solution
			Solution solution = new Solution(input, dict);
			// Solucion to json
			JSONObject jsonSol = solution.solutionToJSON(9945);

			System.out.println(jsonSol);
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}

	}

}
