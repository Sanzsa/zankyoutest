import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Dictionary {

	private ArrayList<String> words = new ArrayList<String>();
	private static String DICT_PATH = "src/dict.txt";

	public Dictionary() {
		super();
		try (BufferedReader br = new BufferedReader(new FileReader(DICT_PATH))) {
			String line;
			while((line = br.readLine()) != null) {
				words.add(line);
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public Dictionary(String path) {
		super();
		try (BufferedReader br = new BufferedReader(new FileReader(path))) {
			String line;
			while((line = br.readLine()) != null) {
				words.add(line);
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public ArrayList<String> getNeighboursWords(String word) {
		ArrayList<String> neighbours = new ArrayList<String>();
		
		for(int i = 0; i < words.size(); i++) {
			int diffs = 0;
			for(int j = 0; j < word.length(); j++) {
				if(word.length() == words.get(i).length())
					if(word.charAt(j) != words.get(i).charAt(j))
						diffs++;
			}
			
			if(diffs == 1)
				neighbours.add(words.get(i));
		}
		return neighbours;
	}
}
