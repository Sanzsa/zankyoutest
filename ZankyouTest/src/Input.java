import org.json.JSONException;
import org.json.JSONObject;


public class Input {

	private int contestId;
	private String startWord;
	private String endWord;

	public Input(JSONObject json) throws JSONException {
		super();
		this.contestId = json.getInt("contestId");
		this.startWord = json.getString("startWord");
		this.endWord = json.getString("endWord");
	}
	
	public String getStartWord() {
		return startWord;
	}
	public void setStartWord(String startWord) {
		this.startWord = startWord;
	}
	public String getEndWord() {
		return endWord;
	}
	public void setEndWord(String endWord) {
		this.endWord = endWord;
	}
	public int getContestId() {
		return contestId;
	}
}
