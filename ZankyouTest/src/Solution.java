import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

public class Solution {

	private ArrayList<String> solution;
	private int contestId;
	private Dictionary dict;
	private static int MAX_DEPTH = 6;

	public Solution(Input input, Dictionary dict) {
		super();
		this.contestId = input.getContestId();
		this.dict = dict;
		this.solution = getSolution(input.getStartWord(), input.getEndWord(), 0);
	}
	
	public ArrayList<String> getSolution(String start, String end, int depth) {
		ArrayList<String> path = new ArrayList<String>();
		
		if(start.equals(end) || depth >= MAX_DEPTH) { // Final condition
			path.add(end);
			return path;
		} else {
			ArrayList<String> neighbours = this.dict.getNeighboursWords(start);
			int min_path = MAX_DEPTH;
			ArrayList<String> aux_path = new ArrayList<String>();
			// For all words with one different letter
			for(int i = 0; i < neighbours.size(); i++) {
				aux_path = this.getSolution(neighbours.get(i), end, depth + 1);
				if(min_path > aux_path.size()) {
					min_path = aux_path.size();
					path = aux_path;
				}
			}
			path.add(0, start);
			if (path.size() == 1)
				return new ArrayList<String>();
			return path;
		}
	}
	
	public JSONObject solutionToJSON(int userId) throws JSONException {
		JSONObject json = new JSONObject();
		json.put("contestId", this.contestId);
		json.put("userId", userId);
		json.put("solution", this.solution);
		
		return json;
	}

	@Override
	public String toString() {
		return solution.toString();
	}

}
