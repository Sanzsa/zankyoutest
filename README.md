# Zankyou Test

Programa que encuentra el camino entre dos palabras, mediante palabras que solo van cambiando una letra pero están en un diccionario.

## Clases

- **Dictionary**: crea un diccionario a partir de un fichero con una lista de palabras, separadas por saltos de línea.
- **Input**: recibe un json como se indica en el enunciado y lo transforma en un elemento que Solution admite como entrada.
- **Solution**: se encarga de calcular la solución al problema, a partir de una entrada Input y un diccionario.
- **Main**: rutina de ejemplo que prueba el algoritmo.
